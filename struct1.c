/*1) Declare una estructura para almacenar
datos de estudiantes (DNI y dos notas correspondientes
a los dos cuatrimestres del año). Haga un programa que
permita ingresar los datos de 5 estudiantes en un vector
de estas estructuras.Luego de ingresar los datos
se deben mostrar los DNI de cada estudiante
y el promedio que tiene en sus exámenes.*/
#include <stdio.h>
#include<conio.h>
#define CANTIDAD_DE_NOTAS 2
#define ESTUDIANTES 5

struct alumno
{
    int dni;
    float notas[CANTIDAD_DE_NOTAS];
};

int main (){
    struct alumno estudiantes[ESTUDIANTES];
    float promedio[ESTUDIANTES];
    int i;
    int j;

    for (i = 0; i < ESTUDIANTES; i++)
    {
        printf("\nIngrese el DNI del %dº alumno: ",i+1);
        scanf("%d", &estudiantes[i].dni);
        for ( j = 0; j < CANTIDAD_DE_NOTAS; j++)//pasa por todas las notas de un mismo estudiante
        {
            printf("\nIngrese la %dº nota: ",j+1);
            scanf("%f",&estudiantes[i].notas[j]);
        }
        
    }

    for (i = 0; i < CANTIDAD_DE_NOTAS; i++)
    {
        for (j = 0; j < CANTIDAD_DE_NOTAS; j++)//pasa por todas las notas de un mismo estudiante
        {
            promedio[i] += estudiantes[i].notas[j];
        }
        
    }
    
    for (i = 0; i < ESTUDIANTES; i++)
    {
        promedio[i] = promedio[i] / CANTIDAD_DE_NOTAS;
        printf("\nEl dni del %dº estudiante es: %d\nEl promedio de sus notas es: %f\n", i+1, estudiantes[i].dni, promedio[i]);
    }
    
    getch();
}