/*2) Una aerolinea tiene vuelos, los cuales 
poseen un código alfanumérico (ejemplo: AR1500),
una ciudad de origen y una ciudad de destino.
Ingrese 4 vuelos en un vector. Luego de ingresados
los datos permita que el usuario escriba el nombre
de una ciudad y muestre los vuelos que parten
y los que arriban a esa ciudad. Si no hubiera 
vuelos para la ciudad ingresada debe mostrarse un mensaje de error.
*/
#include <stdio.h>
#include<conio.h>
#include<string.h>
#define LETRAS 100
#define VUELOS 3

struct vuelos
{
    char codigo[LETRAS];
    char ciudad_origen[LETRAS];
    char ciudad_destino[LETRAS];
};

int main (){
    struct vuelos viajes[VUELOS];
    char ciudad_de_usuario[LETRAS];
    int verificacion_de_vuelos = 0;
    int aux1 = 1;
    int i;

    for ( i = 0; i < VUELOS; i++)
    {
        printf("\nIngrese el codigo alfanumerico del %dº vuelo: ",aux1);
        scanf("%s",viajes[i].codigo);
        printf("\nIngrese la ciudad de origen del %dº vuelo: ",aux1);
        scanf("%s",viajes[i].ciudad_origen);
        printf("\nIngrese la ciudad de destino del %dº vuelo: ",aux1);
        scanf("%s",viajes[i].ciudad_destino);
        aux1++;
    }

    printf("\nIngrese la cuidad de la que desee saber los vuelos: ");
    scanf("%s",ciudad_de_usuario);
    printf("\nLas vuelos que arrivan y parten a la ciudad son:");

    for (i = 0; i < VUELOS; i++)
    {
        if (strcmp(ciudad_de_usuario, viajes[i].ciudad_origen) == 0 || strcmp(ciudad_de_usuario, viajes[i].ciudad_destino) == 0)
        {
            printf("\n%s\nde: %s \na: %s\n",viajes[i].codigo,viajes[i].ciudad_origen,viajes[i].ciudad_destino);
            verificacion_de_vuelos = 1;
        }
    }
    
    if (verificacion_de_vuelos == 0)    printf("\nNo se encontraron vuelos para tu ciudad");
    
    getch();
}
