/*4) Una ferretería tiene un listado de 
facturas emitidas en cierto año, con estos
datos: número de factura, fecha de emisión
(día y mes), nombre del cliente y monto total.
Dado un vector de 10 facturas, se necesita
mostrar en pantalla cuál fue el mejor mes
(o sea, el de mayor dinero facturado). Los
datos se pueden ingresar por teclado o dejarlos
fijos en el programa para no perder tiempo en tipear datos.*/
#include <stdio.h>
#include<conio.h>
#define CANTIDAD_DE_LETRAS 100
#define CANTIDAD_DE_FACTURAS 10

struct fecha
{
    int dia;
    int mes;
};

struct facturas
{
    int numero_factura;
    struct fecha fecha_de_emisión;
    char nombre_del_cliente[CANTIDAD_DE_LETRAS];
    float monto_total;
};

int main (){
    struct facturas facturas_emitidas[CANTIDAD_DE_FACTURAS];
    float mejor_mes = 0;//mes que mas dinero se facturo
    int i;

    for (i = 0; i < CANTIDAD_DE_FACTURAS; i++)
    {
        printf("\nIngrese el numero de la %dº factura:", i+1);
        scanf("%d", &facturas_emitidas[i].numero_factura);
        printf("\nIngrese el dia de emision de la %dº factura:", i+1);
        scanf("%d", &facturas_emitidas[i].fecha_de_emisión.dia);
        printf("\nIngrese el mes de emision de la %dº factura:", i+1);
        scanf("%d", &facturas_emitidas[i].fecha_de_emisión.mes);
        printf("\nIngrese el nombre del cliente de la %dº factura:", i+1);
        scanf("%s", &facturas_emitidas[i].nombre_del_cliente);
        printf("\nIngrese el monto total de la %dº factura:", i+1);
        scanf("%f", &facturas_emitidas[i].monto_total);
        if (facturas_emitidas[i].monto_total > mejor_mes)    mejor_mes = facturas_emitidas[i].monto_total;
        
    }
    
    printf("\nEl mes que se recaudo mas dinero fue: %f", mejor_mes);
    getch();
}
