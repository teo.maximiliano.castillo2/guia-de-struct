/*3) Utilice el ejercicio 1, modificándolo
para que las notas del estudiante estén en
un vector de notas dentro de la estructura. 
Este vector de notas puede almacenar hasta 
10 notas del alumno. Los lugares no utilizados 
se escriben con un valor -1 para indicar que 
ese lugar está vacío. De esta manera, un estudiante
puede haber rendido 2 exámenes, otro 4 y 
otro 10, para citar algunos ejemplos. Se pide
conservar la funcionalidad del programa 
teniendo en cuenta esta nueva organización 
de la información. Por lo tanto, para mostrar 
el promedio del estudiante, habrá que considerar 
las notas que tiene en este vector de notas, 
teniendo cuidado en utilizar solamente las 
notas que existan y omitiendo los -1 que pueda haber.*/
#include <stdio.h>
#include<conio.h>
#define CANTIDAD_DE_NOTAS 10
#define ESTUDIANTES 5

struct alumno
{
    int dni;
    float notas[CANTIDAD_DE_NOTAS];
    float promedio[ESTUDIANTES];
};

int main (){
    struct alumno estudiantes[ESTUDIANTES];
    int cantidad_de_notas[CANTIDAD_DE_NOTAS] = {0};
    int i;
    int j;

    for (i = 0; i < ESTUDIANTES; i++)
    {
        printf("\nIngrese el DNI del %dº alumno: ",i+1);
        scanf("%d", &estudiantes[i].dni);
        for ( j = 0; j < CANTIDAD_DE_NOTAS; j++)//pasa por todas las notas(j) de un mismo estudiante (i)
        {
            printf("\nIngrese la %dº nota: ",j+1);
            scanf("%f",&estudiantes[i].notas[j]);
        }
        
    }

    for (i = 0; i < ESTUDIANTES; i++)
    {
        for (j = 0; j < CANTIDAD_DE_NOTAS; j++)//pasa por todas las notas(j) de un mismo estudiante (i)
        {
            if (estudiantes[i].notas[j] != -1)
            {
                estudiantes[i].promedio[i] += estudiantes[i].notas[j];
                cantidad_de_notas[i]++;
            }
        }
        
    }
    
    for (i = 0; i < ESTUDIANTES; i++)
    {
         estudiantes[i].promedio[i] =  estudiantes[i].promedio[i] / cantidad_de_notas[i];
        printf("\nEl dni del %dº estudiante es: %d\nEl promedio de sus notas es: %f\n", i+1, estudiantes[i].dni,  estudiantes[i].promedio[i]);
    }
    
    getch();
}
